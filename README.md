# Roble

Roble is a extremely small CSS framework for demo purposes only.
Don't use it in production.

### Install
Drop this in header:
```html
<link href="https://unpkg.com/roble/dist/roble.css" rel="stylesheet" />
```

### Usage

###### Button
```html
<button class="rbl-button">Button</button>
<button class="rbl-button--secondary">Button</button>
```

###### Checkbox
```html
<input id="demo-checkbox" type="checkbox" class="rbl-checkbox">
<label for="demo-checkbox">Checkbox</label>
```

###### Select
```html
<label for="fruits" class="rbl-field-label">Select a fruit</label>
<select id="fruits" title="Gyümik" class="rbl-select">
  <option>orange</option>
  <option>grapefruit</option>
  <option>mango</option>
</select>
```

###### Textfield
```html
<label for="full-name" class="rbl-field-label">Your fantastic name</label>
<input id="full-name" title="Name" class="rbl-textfield" placeholder="Name..."/>
```

###### Panel
```html
<div class="rbl-panel">
<h3 class="rbl-panel__title">Lorem ipsum</h3>
<div class="rbl-panel__inner">
  Lorem ipsum
</div>
</div>
```

### Releasing

npm run release [major|minor|patch]
